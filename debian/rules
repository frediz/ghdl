#!/usr/bin/make -f

# Currently gcc can not be compiled with format-security and it is not needed
# for Ada code anyway
export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-format

#export DH_VERBOSE = 1

include /usr/share/dpkg/default.mk
include /usr/share/ada/debian_packaging-9.mk

# This variable is used in Makefile.in to adjust src/version.in
export PKG_VERSION := $(DEB_VENDOR) $(DEB_VERSION)

# These variables are used to find and unpack the gcc source
GCC_DIR := /usr/src/gcc-9
GCC_VER := 9
GCC_TARBALL := $(notdir $(wildcard $(GCC_DIR)/gcc-*.tar.*))

# Get parallel option to parallelize the gcc build specifically (Ada builds
# are already parallelized by code included from debian_packaging-*.mk above).
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    MAKEPARALLEL = -j$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
endif

# build profile control over what backends get built
ifeq ($(filter pkg.ghdl.nomcode,$(DEB_BUILD_PROFILES)),)
    WITH_MCODE := 1
endif
ifeq ($(filter pkg.ghdl.nogcc,$(DEB_BUILD_PROFILES)),)
    WITH_GCC   := 1
endif
ifeq ($(filter pkg.ghdl.nollvm,$(DEB_BUILD_PROFILES)),)
    WITH_LLVM  := 1
endif

# force disable mcode build on non-x86 architectures where it isn't supported
ifeq ($(filter amd64 i386,$(DEB_HOST_ARCH_CPU)),)
    WITH_MCODE :=
endif

BUILDDIR := $(CURDIR)/builddir
CC := gnatgcc
export CC


%:
	dh ${@}

override_dh_missing:
	dh_missing --list-missing

override_dh_auto_configure:
	mkdir -p $(BUILDDIR)/mcode $(BUILDDIR)/llvm $(BUILDDIR)/gcc
	@echo
	@echo ------------------------------------------------------------
	@echo Configuring with mcode backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_MCODE)" ]; then \
		cd $(BUILDDIR)/mcode; \
		../../configure --srcdir=../.. --prefix=/usr \
			--libdir=lib/ghdl/mcode --incdir=lib/ghdl/include \
			--enable-openieee; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Configuring with llvm backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_LLVM)" ]; then \
		cd $(BUILDDIR)/llvm; \
		../../configure --srcdir=../.. --prefix=/usr \
			--libdir=lib/ghdl/llvm --incdir=lib/ghdl/include \
			--enable-openieee --with-llvm-config; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Configuring with gcc backend
	@echo ------------------------------------------------------------
	# gcc unpack sequence cribbed from gcc-7-cross debian/rules
	set -e; \
	if [ -n "$(WITH_GCC)" ]; then \
		cd $(BUILDDIR)/gcc; \
		ln -sf ${GCC_DIR}/$(GCC_TARBALL) $(GCC_TARBALL); \
		cp -a  ${GCC_DIR}/debian/ .; \
		if [ -n "$$(grep -v '^\#' ${CURDIR}/debian/patches/gcc-${GCC_VER}/series)" ]; then \
			cp -n ${CURDIR}/debian/patches/gcc-${GCC_VER}/*.diff debian/patches/ ; \
			cat ${CURDIR}/debian/patches/gcc-${GCC_VER}/series >> debian/patches/series ; \
		fi; \
		debian/rules patch; \
		../../configure --srcdir=../.. --prefix=/usr \
			--libdir=lib/ghdl/gcc --incdir=lib/ghdl/include \
			--enable-openieee --with-gcc=src; \
		make copy-sources; \
		mkdir gccbuild; \
		cd gccbuild; \
		../src/configure --prefix=/usr/lib/ghdl/gcc --enable-languages=vhdl \
			--enable-default-pie \
			--disable-bootstrap --disable-lto --disable-multilib \
			--disable-libssp --disable-libgomp --disable-libquadmath \
			--with-system-zlib --without-isl; \
	fi

override_dh_auto_build:
	@echo
	@echo ------------------------------------------------------------
	@echo Building with mcode backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_MCODE)" ]; then \
		$(MAKE) -C $(BUILDDIR)/mcode; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Building with llvm backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_LLVM)" ]; then \
		$(MAKE) -C $(BUILDDIR)/llvm; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Building with gcc backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_GCC)" ]; then \
		$(MAKE) $(MAKEPARALLEL) -C $(BUILDDIR)/gcc/gccbuild; \
		$(MAKE) -C $(BUILDDIR)/gcc lib/ghdl/gcc/libgrt.a all.vpi; \
		$(MAKE) -C $(BUILDDIR)/gcc ghdllib \
			GHDL_GCC_BIN=$(BUILDDIR)/gcc/gccbuild/gcc/ghdl \
			GHDL1_GCC_BIN="--GHDL1=$(BUILDDIR)/gcc/gccbuild/gcc/ghdl1"; \
	fi

override_dh_auto_install:
	install -pD debian/ghdl.wrapper $(CURDIR)/debian/tmp/usr/bin/ghdl
	@echo
	@echo ------------------------------------------------------------
	@echo Installing with mcode backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_MCODE)" ]; then \
		$(MAKE) -C $(BUILDDIR)/mcode install DESTDIR=../../debian/tmp; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Installing with llvm backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_LLVM)" ]; then \
		$(MAKE) -C $(BUILDDIR)/llvm install DESTDIR=../../debian/tmp; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Installing with gcc backend
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_GCC)" ]; then \
		$(MAKE) -C $(BUILDDIR)/gcc/gccbuild install \
			DESTDIR=$(CURDIR)/debian/tmp; \
		$(MAKE) -C $(BUILDDIR)/gcc install DESTDIR=$(CURDIR)/debian/tmp; \
		mv debian/tmp/usr/lib/ghdl/gcc/bin/ghdl-gcc debian/tmp/usr/bin/ghdl-gcc; \
		mv debian/tmp/usr/lib/ghdl/gcc/lib/ghdl/libbacktrace.a debian/tmp/usr/lib/ghdl/gcc/vhdl/libbacktrace.a; \
	fi
	@echo
	@echo ------------------------------------------------------------
	@echo Moving parts to required locations
	@echo ------------------------------------------------------------
	if [ -n "$(WITH_MCODE)" ]; then \
		$(RM) -r debian/tmp/usr/lib/ghdl/src; \
		mv debian/tmp/usr/lib/ghdl/mcode/vhdl/src debian/tmp/usr/lib/ghdl; \
		ln -s ../../src debian/tmp/usr/lib/ghdl/mcode/vhdl/src; \
	fi
	if [ -n "$(WITH_LLVM)" ]; then \
		$(RM) -r debian/tmp/usr/lib/ghdl/src; \
		mv debian/tmp/usr/lib/ghdl/llvm/vhdl/src debian/tmp/usr/lib/ghdl; \
		ln -s ../../src debian/tmp/usr/lib/ghdl/llvm/vhdl/src; \
	fi
	if [ -n "$(WITH_GCC)" ]; then \
		$(RM) -r debian/tmp/usr/lib/ghdl/src; \
		mv debian/tmp/usr/lib/ghdl/gcc/vhdl/src debian/tmp/usr/lib/ghdl; \
		ln -s ../../src debian/tmp/usr/lib/ghdl/gcc/vhdl/src; \
	fi
